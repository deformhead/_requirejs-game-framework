#!/bin/bash

# line feed
LF='\n'

# vendors path
PATH_VENDORS='./vendors'

# requirejs url
URL_REQUIREJS='http://requirejs.org/docs/release/2.1.22/minified/require.js'

# download requirejs vendor
curl $URL_REQUIREJS --output $PATH_VENDORS'/require.js' --silent --write-out '%{http_code} GET '$PATH_VENDORS'/require.js'$LF
