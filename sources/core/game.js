define(

    [ 'core/loop', 'core/scene' ],

    function module( Loop, Scene ) {

        'use strict' ;

        var Game ;

        Game = function Game( target ) {

            var initialize ;
            var loop ;
            var render ;
            var scene ;
            var update ;

            initialize = function initialize( target ) {

                // start a new loop instance
                loop = new Loop( ) ;

                // start a new scene instance with wished target
                scene = new Scene( target ) ;

                // start updating this game's instance
                loop.update( update ) ;

                // start rendering this game's instance
                loop.render( render ) ;
            } ;

            render = function render( ) {

                // render this game's current scene
                scene.render( ) ;
            } ;

            update = function update( ) {

                // update this game's current scene
                scene.update( 1000 / loop.framerate ) ;
            } ;

            // initialize this instance
            initialize( target ) ;
        } ;

        return ( Game ) ;
    }
) ;
