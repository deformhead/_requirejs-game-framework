define(

    function module( ) {

        'use strict' ;

        var Scene ;

        Scene = function Scene( target ) {

            var current ;
            var initialize ;
            var load ;
            var started ;

            Scene.prototype.render = function render( ) {

                // if current scene is enabled
                if ( started === true ) {

                    // render current scene
                    current.render( ) ;
                }
            } ;

            Scene.prototype.update = function update( delta ) {

                // if current scene is enabled
                if ( started === true ) {

                    // update current scene
                    current.update( delta ) ;

                    // if current scene's target is defined
                    if ( current.target !== null ) {

                        // load target scene
                        load( current.target ) ;
                    }
                }
            } ;

            initialize = function initialize( target ) {

                // load target scene
                load( target ) ;
            } ;

            load = function load( target ) {

                var Data ;
                var Scene ;

                // disable current scene
                started = false ;

                Data = target.data ;
                Scene = target.scene ;

                // define wished scene with its data
                current = new Scene( Data ) ;

                // enable the new scene
                started = true ;
            } ;

            // initialize this instance
            initialize( target ) ;
        } ;

        return ( Scene ) ;
    }
) ;
