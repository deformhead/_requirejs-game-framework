define(

    [ ],

    function module( ) {

        'use strict' ;

        var Hello ;

        Hello = function Hello( ) {

            this.hello = 'hello world' ;
            this.count = 0 ;
        } ;

        return ( Hello ) ;
    }
) ;
