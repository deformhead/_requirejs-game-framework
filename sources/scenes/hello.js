define(

    [ ],

    function module( ) {

        'use strict' ;

        var Hello ;

        Hello = function Hello( Data ) {

            var data ;
            var initialize ;

            Hello.prototype.render = function render( ) {

                console.log( data.hello, data.count / 1000 ) ;
            } ;

            Hello.prototype.update = function update( delta ) {

                data.count += delta ;
            } ;

            initialize = function initialize( Data ) {

                this.target = null ;

                data = new Data( ) ;

            // keep the context
            }.bind( this ) ;

            // initialize this instance
            initialize( Data ) ;
        } ;

        return ( Hello ) ;
    }
) ;
