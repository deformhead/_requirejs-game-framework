( function scope( ) {

    'use strict' ;

    var config ;
    var dependencies ;

    // define user's path configuration
    config = {

        'baseUrl': './',

        'paths': {

            'core': './core',
            'data': './data',
            'helpers': './helpers',
            'scenes': './scenes'
        }
    } ;

    // define user's first scene with its data
    dependencies = [

        'scenes/hello',
        'data/hello'
    ] ;

    require(

        config,

        [ 'core/game' ].concat( dependencies ),

        function module( Game, Scene, Data ) {

            var target ;

            target = {

                'scene': Scene,
                'data': Data
            } ;

            // start user's game
            new Game( target ) ;
        }
    ) ;

// IIFE
} )( ) ;
